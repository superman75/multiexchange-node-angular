import {Component, OnInit, AfterViewInit} from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.scss']
})
export class MainComponent implements OnInit, AfterViewInit {

  cur_menu;
  sub_menu = [];

  constructor(private _router: Router) {
  }

  ngOnInit() {
    this.cur_menu = 0;
    this.sub_menu = ['Markets', 'Orders', 'Wallets', 'Settings', 'Logout'];
  }

  ngAfterViewInit() {
    const menus = document.getElementsByClassName('sub-menu')[0].children;
    (<HTMLSpanElement>menus[this.cur_menu]).classList.add('shadow-text');
  }

  changeSubmenu(menu_index) {
    this.cur_menu = menu_index;
    const menus = document.getElementsByClassName('sub-menu')[0].children;

    for (let i = 0; i < menus.length; i++) {
      if (i === menu_index) {
        (<HTMLSpanElement>menus[i]).classList.add('shadow-text');
      } else {
        (<HTMLSpanElement>menus[i]).classList.remove('shadow-text');
      }
    }
  }

  goLand() {
    this._router.navigate(['']);
  }

}
