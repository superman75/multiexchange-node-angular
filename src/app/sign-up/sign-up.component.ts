import { Component, OnInit } from '@angular/core';
import {User} from "../user.model";
import {Router} from "@angular/router";
import {DataService} from "../data.service";
import {HttpClient, HttpHeaders} from "@angular/common/http";

import * as $ from 'jquery';
// import {AlertsService} from "angular-alert-module";

// const httpOptions = {
//   headers: new HttpHeaders({
//     'Content-Type':  'application/json'
//   })
// };
@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.scss']
})
export class SignUpComponent implements OnInit {

  user = {email : '', password : '', vpassword : ''};
  data : any;
  alertMessage: string;
  alertInstance: any;
  hiddenElement : boolean = true;

  constructor(private _router:Router, private http :HttpClient,
              // private alerts : AlertsService,
              private dabaService : DataService) { }

  ngOnInit() {
    this.alertInstance = $("#success-alert");
  }

  displayAlert(msg){
    this.alertMessage = msg;
    this.hiddenElement = false;
    setTimeout(()=>{
      this.hiddenElement = true;
    }, 3000);
    // this.alertInstance.delay(4000).slideUp(200, function() {
    //
    // });
    // this.alertInstance.fadeTo(2000, 500).slideUp(500, function(){
    //   $("#success-alert").slideUp(500);
    // });
  }

  goEmailVerify(){
    // this.alerts.setDefaults('timeout', 3);

     console.log(this.user);
     if (this.user.email == '' || this.user.password == ''){
       this.displayAlert("Please insert your email and password");
       return;
     }
     if(this.user.password !== this.user.vpassword) {

       this.displayAlert("Password is not matched.");
       return;
     }

     this.dabaService.signup(this.user).subscribe(res=>{
       this.data = res;
       if(this.data.status =='Failed'){
         this.displayAlert(this.data.msg);
       }
       else{
         this._router.navigate(['emailverify']);
       }
     });
  }

  goLogin() {
    this._router.navigate(['login']);
  }

}
