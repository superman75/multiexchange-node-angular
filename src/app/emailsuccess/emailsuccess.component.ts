import { Component, OnInit } from '@angular/core';
import {Router} from "@angular/router";

@Component({
  selector: 'app-emailsuccess',
  templateUrl: './emailsuccess.component.html',
  styleUrls: ['./emailsuccess.component.scss']
})
export class EmailsuccessComponent implements OnInit {

  verifyCode : string;
  data :any;
  constructor(private _router:Router) {
  }

  ngOnInit() {
  }

  goLogIn(){
    this._router.navigate(['login']);
  }

}
