import {Component, OnInit, AfterViewInit, ChangeDetectorRef} from '@angular/core';
import {DataService} from '../data.service';

import * as $ from 'jquery';
import 'datatables.net';
import 'datatables.net-bs4';

@Component({
  selector: 'app-order',
  templateUrl: './order.component.html',
  styleUrls: ['./order.component.scss']
})
export class OrderComponent implements OnInit, AfterViewInit {

  coin_selected;
  cur_market;
  coin_markets = [];
  cur_coin_list = [];
  open_orders = [];
  order_history = [];
  sell_list = [];
  buy_list = [];
  total_sell;
  total_buy;
  order_button_label;
  order_button_style;
  open_orders_tb: any;
  order_history_tb: any;
  sell_coin_tb: any;
  buy_coin_tb: any;
  selected_coin: any;
  recent_trade;

  constructor(private _data: DataService, private chRef: ChangeDetectorRef) {
  }

  ngOnInit() {
    this.coin_selected = false;

    this.coin_markets = [
      {
        id: 1,
        label: 'BTC Markets'
      }, {
        id: 2,
        label: 'ETH Markets'
      }, {
        id: 3,
        label: 'AFCash Markets'
      }
    ];

    this.open_orders = [
      {
        order_date: '06/12/2017',
        order_market: 'BTC-ETH',
        order_type: '-',
        order_bid: '-',
        order_filled: '-',
        unit_total: '-',
        actual_rate: '-',
        est_total: '-'
      }
    ];

    this.order_history = [];

    this.total_sell = 0;
    this.total_buy = 0;

    this.order_button_label = 'Sell';
    this.order_button_style = 'btn btn-sm btn-success col-8 offset-2';

    this.sell_list = [
      {
        sum: 0.123,
        total: 0.123,
        size: 145,
        bid: 0.000301
      }, {
        sum: 0.464,
        total: 0.341,
        size: 854,
        bid: 0.000241
      }, {
        sum: 1.212,
        total: 0.748,
        size: 365,
        bid: 0.000111
      }, {
        sum: 1.212,
        total: 0.748,
        size: 365,
        bid: 0.000111
      }, {
        sum: 1.212,
        total: 0.748,
        size: 365,
        bid: 0.000111
      }, {
        sum: 1.212,
        total: 0.748,
        size: 365,
        bid: 0.000111
      }, {
        sum: 1.212,
        total: 0.748,
        size: 365,
        bid: 0.000111
      }, {
        sum: 1.212,
        total: 0.748,
        size: 365,
        bid: 0.000111
      }, {
        sum: 1.212,
        total: 0.748,
        size: 365,
        bid: 0.000111
      }, {
        sum: 1.212,
        total: 0.748,
        size: 365,
        bid: 0.000111
      }
    ];

    this.buy_list = [
      {
        sum: 0.314,
        total: 0.314,
        size: 234,
        ask: 0.000231
      }, {
        sum: 0.679,
        total: 0.365,
        size: 365,
        ask: 0.000201
      }, {
        sum: 0.963,
        total: 0.284,
        size: 784,
        ask: 0.000131
      }, {
        sum: 0.314,
        total: 0.314,
        size: 234,
        ask: 0.000231
      }, {
        sum: 0.679,
        total: 0.365,
        size: 365,
        ask: 0.000201
      }, {
        sum: 0.963,
        total: 0.284,
        size: 784,
        ask: 0.000131
      }, {
        sum: 0.314,
        total: 0.314,
        size: 234,
        ask: 0.000231
      }, {
        sum: 0.679,
        total: 0.365,
        size: 365,
        ask: 0.000201
      }, {
        sum: 0.963,
        total: 0.284,
        size: 784,
        ask: 0.000131
      }, {
        sum: 0.963,
        total: 0.284,
        size: 784,
        ask: 0.000131
      }
    ];

    this.recent_trade = [
      {
        type: 'sell',
        time: '7hours ago',
        price: '270,000.01',
        amount: '0.003'
      }, {
        type: 'sell',
        time: '7hours ago',
        price: '270,000.01',
        amount: '0.003'
      }, {
        type: 'buy',
        time: '7hours ago',
        price: '270,000.01',
        amount: '0.003'
      }, {
        type: 'sell',
        time: '7hours ago',
        price: '270,000.01',
        amount: '0.003'
      }, {
        type: 'buy',
        time: '7hours ago',
        price: '270,000.01',
        amount: '0.003'
      }, {
        type: 'sell',
        time: '7hours ago',
        price: '270,000.01',
        amount: '0.003'
      }, {
        type: 'buy',
        time: '7hours ago',
        price: '270,000.01',
        amount: '0.003'
      }, {
        type: 'sell',
        time: '7hours ago',
        price: '270,000.01',
        amount: '0.003'
      }, {
        type: 'buy',
        time: '7hours ago',
        price: '270,000.01',
        amount: '0.003'
      }, {
        type: 'sell',
        time: '7hours ago',
        price: '270,000.01',
        amount: '0.003'
      }, {
        type: 'buy',
        time: '7hours ago',
        price: '270,000.01',
        amount: '0.003'
      }, {
        type: 'buy',
        time: '7hours ago',
        price: '270,000.01',
        amount: '0.003'
      }, {
        type: 'sell',
        time: '7hours ago',
        price: '270,000.01',
        amount: '0.003'
      }, {
        type: 'sell',
        time: '7hours ago',
        price: '270,000.01',
        amount: '0.003'
      }, {
        type: 'sell',
        time: '7hours ago',
        price: '270,000.01',
        amount: '0.003'
      }, {
        type: 'buy',
        time: '7hours ago',
        price: '270,000.01',
        amount: '0.003'
      }, {
        type: 'buy',
        time: '7hours ago',
        price: '270,000.01',
        amount: '0.003'
      }, {
        type: 'sell',
        time: '7hours ago',
        price: '270,000.01',
        amount: '0.003'
      }, {
        type: 'sell',
        time: '7hours ago',
        price: '270,000.01',
        amount: '0.003'
      }, {
        type: 'sell',
        time: '7hours ago',
        price: '270,000.01',
        amount: '0.003'
      }, {
        type: 'sell',
        time: '7hours ago',
        price: '270,000.01',
        amount: '0.003'
      }, {
        type: 'buy',
        time: '7hours ago',
        price: '270,000.01',
        amount: '0.003'
      }, {
        type: 'sell',
        time: '7hours ago',
        price: '270,000.01',
        amount: '0.003'
      }, {
        type: 'buy',
        time: '7hours ago',
        price: '270,000.01',
        amount: '0.003'
      }
    ];
  }

  ngAfterViewInit() {
    jQuery('select').selectpicker();

    this.chRef.detectChanges();
    const table1: any = $('#open-orders');
    this.open_orders_tb = table1.DataTable({pagingType: 'full_numbers', destroy: true});

    const table2: any = $('#order-history');
    this.order_history_tb = table2.DataTable({pagingType: 'full_numbers', destroy: true});
  }

  onChangeMarket(market_id) {
    switch (market_id) {
      case '1':
        this.cur_coin_list = this._data.btc_markets;
        this.cur_market = 'BTC';
        break;
      case '2':
        this.cur_coin_list = this._data.eth_markets;
        this.cur_market = 'ETH';
        break;
    }
  }

  onChangeCoin(coin_index) {
    this.coin_selected = true;
    this.selected_coin = this.cur_coin_list[coin_index];

    this.chRef.detectChanges();
    const table3: any = $('#sell-coin');
    this.sell_coin_tb = table3.DataTable({pagingType: 'full_numbers', destroy: true});

    const table4: any = $('#buy-coin');
    this.buy_coin_tb = table4.DataTable({pagingType: 'full_numbers', destroy: true});
  }

  onFlagChange(flag) {
    if (flag) {
      this.order_button_label = 'Sell';
      this.order_button_style = 'btn btn-sm btn-success col-8 offset-2';
    } else {
      this.order_button_label = 'Buy';
      this.order_button_style = 'btn btn-sm btn-danger col-8 offset-2';
    }
  }
}
