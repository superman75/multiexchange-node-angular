export class User {
  firstname: string;
  lastname: string;
  country: string;
  username: string;
  email: string;
  password: string;
  confirmpassword: string;
}
