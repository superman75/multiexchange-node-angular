import {Component, OnInit} from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-land',
  templateUrl: './land.component.html',
  styleUrls: ['./land.component.scss']
})
export class LandComponent implements OnInit {

  constructor(private _router: Router) {
  }

  ngOnInit() {
  }

  goMain() {
    this._router.navigate(['main']);
  }

  goSignUp() {
    this._router.navigate(['sign_up']);
  }
  goLogIn(){
    this._router.navigate( ['login']);
  }

}
