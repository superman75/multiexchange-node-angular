import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {HttpClientModule} from '@angular/common/http';
import {BootstrapSwitchModule} from 'angular2-bootstrap-switch';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';

import { AppRoutingModule } from './app-routing.module';

import { DataService } from './data.service';

import { AppComponent } from './app.component';
import { MarketComponent } from './market/market.component';
import { MainComponent } from './main/main.component';
import { OrderComponent } from './order/order.component';
import { WalletComponent } from './wallet/wallet.component';
import { SettingsComponent } from './settings/settings.component';
import { LandComponent } from './land/land.component';
import { SignUpComponent } from './sign-up/sign-up.component';
import { GeneralComponent } from './settings/general/general.component';

import {FormsModule} from "@angular/forms";
import { LoginComponent } from './login/login.component';
import { WhitelistComponent } from './settings/whitelist/whitelist.component';
import { SecurityComponent } from './settings/security/security.component';
import { EmailverifyComponent } from './emailverify/emailverify.component';
import { ApikeysComponent } from './settings/apikeys/apikeys.component';
import { VerificationComponent } from './settings/verification/verification.component';
import { EmailsuccessComponent } from './emailsuccess/emailsuccess.component';

// import {AlertsModule, AlertsService} from "angular-alert-module";


@NgModule({
  declarations: [
    AppComponent,
    MarketComponent,
    MainComponent,
    OrderComponent,
    WalletComponent,
    SettingsComponent,
    LandComponent,
    SignUpComponent,
    LoginComponent,
    GeneralComponent,
    SecurityComponent,
    EmailverifyComponent,
    WhitelistComponent,
    ApikeysComponent,
    VerificationComponent,
    EmailsuccessComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    BootstrapSwitchModule,
    BrowserAnimationsModule,
    // AlertsModule.forRoot(),
    FormsModule
  ],
  providers: [DataService],
  bootstrap: [AppComponent]
})
export class AppModule { }
