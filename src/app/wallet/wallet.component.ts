import {ChangeDetectorRef, Component, OnInit} from '@angular/core';

import * as $ from 'jquery';
import 'datatables.net';
import 'datatables.net-bs4';

@Component({
  selector: 'app-wallet',
  templateUrl: './wallet.component.html',
  styleUrls: ['./wallet.component.scss']
})
export class WalletComponent implements OnInit {

  dataTable: any;
  dataTable1: any;
  dataTable2: any;
  dataTable3: any;
  dataTable4: any;

  walletRecord: any[];
  withdrawals: any[];
  deposits: any[];
  withhists: any[];
  dephists: any[];

  pendWithPlus = true;
  pendDepPlus = true;
  withHistPlus = true;
  depHistPlus = true;

  constructor(private chRef: ChangeDetectorRef) {
  }

  ngOnInit() {

    this.walletRecord = [{
      currency_name: 'Bitcoin',
      symbol: 'BTC',
      avail_balance: 0.00000000,
      pending_deposit: 0.00000000,
      reserved: 0.00000000,
      total: 0.00000000,
      est_btc: 0.00000000,
      change: 0.0
    }, {
      currency_name: 'Bitcoin Cash',
      symbol: 'BCH',
      avail_balance: 0.00000000,
      pending_deposit: 0.00010000,
      reserved: 0.00000000,
      total: 0.00000000,
      est_btc: 0.00000000,
      change: 0.0
    }
    ];
    this.chRef.detectChanges();
    const table: any = $('table');
    this.dataTable = table.DataTable({pagingType: 'full_numbers', destroy: true});

  }

  clickPendWith() {
    if (this.pendWithPlus == true) {
      this.pendWithPlus = false;
      this.withdrawals = [{
        date: '5/31/2018',
        symbol: 'BTC',
        quantity: 5,
        status: 'completed'
      }
      ];
      this.chRef.detectChanges();
      const table: any = $('#withdraw_table');
      this.dataTable1 = table.DataTable({pagingType: 'full_numbers', destroy: true});
    } else {
      this.pendWithPlus = true;
    }
  }

  clickPendDep() {
    if (this.pendDepPlus == true) {
      this.pendDepPlus = false;
      this.deposits = [{
        date: '6/31/2018',
        symbol: 'ETH',
        quantity: 0.13,
        status: 'pending'
      }
      ];
      this.chRef.detectChanges();
      const table: any = $('#deposit_table');
      this.dataTable2 = table.DataTable({pagingType: 'full_numbers', destroy: true});
    } else {
      this.pendDepPlus = true;
    }
  }

  clickWithHist() {
    if (this.withHistPlus == true) {
      this.withHistPlus = false;
      this.withhists = [{
        date: '6/31/2018',
        symbol: 'ETH',
        quantity: 0.13,
        status: 'pending'
      }
      ];
      this.chRef.detectChanges();
      const table: any = $('#withhist_table');
      this.dataTable3 = table.DataTable({pagingType: 'full_numbers', destroy: true});
    } else {
      this.withHistPlus = true;
    }
  }

  clickDepHist() {
    if (this.depHistPlus == true) {
      this.depHistPlus = false;
      this.dephists = [{
        date: '6/31/2018',
        symbol: 'ETH',
        quantity: 0.13,
        status: 'pending'
      }
      ];
      this.chRef.detectChanges();
      const table: any = $('#dephist_table');
      this.dataTable4 = table.DataTable({pagingType: 'full_numbers', destroy: true});
    } else {
      this.depHistPlus = true;
    }
  }

}
