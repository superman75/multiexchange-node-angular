import {AfterViewInit, ChangeDetectorRef, Component, OnInit} from '@angular/core';
import * as $ from 'jquery';
import 'datatables.net';
import 'datatables.net-bs4';
import {HttpClient} from '@angular/common/http';
import { DataService} from "../data.service";
import 'rxjs/Rx';

@Component({
  selector: 'app-market',
  templateUrl: './market.component.html',
  styleUrls: ['./market.component.scss']
})
export class MarketComponent implements OnInit {


  USDTable: any[];
  BTCTable: any[];
  ETHTable: any[];
  USDTTable: any[];
  temp : any;
  totalVolumnOfUSD = 1009805.480;
  lastPriceOfBTC = 6286.29200000;
  hrVolumnOfBTC = 693449.38;
  totalVolumnOfBTC = 1009805.480;
  totalVolumnOfETH = 1009805.480;
  totalVolumnOfUSDT = 1009805.480;

  dataTable: any;

  constructor(private http: HttpClient, private chRef: ChangeDetectorRef,
              private dataService : DataService) {

    this.USDTable = [];
    this.BTCTable = [];
    this.ETHTable = [];
    this.USDTTable = [];
  }

  ngOnInit(): void {

    this.dataService.getMarket().subscribe(data=>{
      //console.log(res);
      this.temp = data;
      if(this.temp.success){
        this.temp.result.forEach(value=>{
          if(value.MarketName.split('-')[0]=='USD')this.USDTable.push(value);
          if(value.MarketName.split('-')[0]=='BTC')this.BTCTable.push(value);
          if(value.MarketName.split('-')[0]=='ETH')this.ETHTable.push(value);
          if(value.MarketName.split('-')[0]=='USDT')this.USDTTable.push(value);

        });
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                      
        this.chRef.detectChanges();
        const table: any = $('table');
        this.dataTable = table.DataTable({pagingType : 'full_numbers', destroy: true});
      }
    });


  }

}
