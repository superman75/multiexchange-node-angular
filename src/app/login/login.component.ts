import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import {Router} from "@angular/router";
import { DataService} from "../data.service";
// import {AlertsService} from "angular-alert-module";

import * as $ from 'jQuery';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  user = {
    email : '',
    password : ''
  };
  data : any;
  alertMessage: string;
  alertInstance: any;
  hiddenElement : boolean = true;

  constructor(private _router:Router,
              private http:HttpClient,
              // private alerts : AlertsService,
              private dataService : DataService) { }

  ngOnInit() {
    this.alertInstance = $("#success-alert");
  }

  displayAlert(msg){
    this.alertMessage = msg;
    this.hiddenElement = false;
    setTimeout(()=>{
      this.hiddenElement = true;
    }, 3000);
    // this.alertInstance.delay(4000).slideUp(200, function() {
    //
    // });
    // this.alertInstance.fadeTo(2000, 500).slideUp(500, function(){
    //   $("#success-alert").slideUp(500);
    // });
  }

  goMain(){

    // this.alerts.setDefaults('timeout', 3);

    if (this.user.email == '' || this.user.password == ''){

      this.displayAlert("Please insert your email and password");
      // console.log('Please insert your email and password', 'error');
      return;
    }
    this.dataService.login(this.user).subscribe(res=>{
      this.data = res;
      if(this.data.status=='Failed')  {
        this.displayAlert(this.data.msg);
        // console.log(this.data.msg, 'error');
      }
      else {
        if(this.data.msg.email_verified)this._router.navigate(['main']);
        this.displayAlert("This email is not verified");
        // console.log('This email is not verified', 'error');
      }
      //
    });

  }

  goSignup() {
    this._router.navigate(['sign_up']);
  }

}
