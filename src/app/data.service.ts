import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from "@angular/common/http";


const base_url = 'http://35.178.187.89:8080/api';
const chain_url = 'https://chain.so/api/v2';
@Injectable()
export class DataService {

  btc_markets = [
    {
      market: 'BTC-ADA',
      currency: 'Ada',
      coin: 'ADA',
      volume: 386.99,
      change: -0.2,
      last_price: 0.00001849,
      day_high: 0.00001919,
      day_low: 0.00001825,
      spread: 0.3,
      added: '09/29/2017'
    },
    {
      market: 'BTC-ZCL',
      currency: 'Zclassic',
      coin: 'ZCL',
      volume: 350.79,
      change: 5.3,
      last_price: 0.00220507,
      day_high: 0.00234697,
      day_low: 0.00213016,
      spread: 0.8,
      added: '11/16/2016'
    },
    {
      market: 'BTC-XRP',
      currency: 'XRP',
      coin: 'XRP',
      volume: 279.88,
      change: -3.7,
      last_price: 0.00005245,
      day_high: 0.00005449,
      day_low: 0.00005146,
      spread: 0.4,
      added: '12/23/2014'
    }
  ];

  eth_markets = [
    {
      market: 'BTC-XEM',
      currency: 'NEM',
      coin: 'XEM',
      volume: 256.36,
      change: -0.7,
      last_price: 0.00231219,
      day_high: 0.00221964,
      day_low: 0.00241875,
      spread: 0.5,
      added: '06/10/2016'
    },
    {
      market: 'BTC-ZRX',
      currency: 'Ox Protocol',
      coin: 'ZRX',
      volume: 139.39,
      change: -3.1,
      last_price: 0.01632467,
      day_high: 0.01544847,
      day_low: 0.01723616,
      spread: 0.2,
      added: '10/08/2017'
    },
    {
      market: 'BTC-GNT',
      currency: 'Golem',
      coin: 'GNT',
      volume: 632.14,
      change: -3.8,
      last_price: 0.01135245,
      day_high: 0.01265449,
      day_low: 0.01085146,
      spread: 0.4,
      added: '06/11/2014'
    },
    {
      market: 'BTC-ADA',
      currency: 'Ada',
      coin: 'ADA',
      volume: 386.99,
      change: -0.2,
      last_price: 0.00001849,
      day_high: 0.00001919,
      day_low: 0.00001825,
      spread: 0.3,
      added: '09/29/2017'
    }
  ];


  constructor(private http: HttpClient){}
  signup(user){
     return this.http.post(base_url+'/signup',user);
  }
  login(user){
    console.log(user);
    return this.http.get(base_url+'/login?email=' + user.email + '&password=' + user.password);
  }
  getMarket(){
    var header  = new HttpHeaders();
    header.append('Access-Control-Allow-Origin' , '*');
    header.append('content-type','application/x-www-form-urlencoded');
    header.append('Access-Control-Allow-Headers','application/x-www-form-urlencoded');
    header.append('X-Content-Type-Options','nosniff');

    return this.http.get('https://bittrex.com/api/v1.1/public/getmarketsummaries',{headers: header});
    // return this.http.get('https://shapeshift.io/rate/btc_ltc');
  }
  emailVerify(verifyCode){
    return this.http.get(base_url+'/activate/'+verifyCode);
  }

}
