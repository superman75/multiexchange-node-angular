import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-general',
  templateUrl: './general.component.html',
  styleUrls: ['./general.component.scss']
})
export class GeneralComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

  change(){
    const menu = document.getElementsByClassName('field field_text-small form_profile__disabled')[0] as HTMLInputElement;
    menu.disabled = false;
    // <HTMLSpanElement>menus).classList.add('shadow-text');
  }
}
