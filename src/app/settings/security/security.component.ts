import {ChangeDetectorRef, Component, OnInit, ViewChild} from '@angular/core';
import {MatPaginator, MatSort, MatTableDataSource} from '@angular/material';

import * as $ from 'jquery';
import 'datatables.net';
import 'datatables.net-bs4';

@Component({
  selector: 'app-security',
  templateUrl: './security.component.html',
  styleUrls: ['./security.component.scss']
})
export class SecurityComponent implements OnInit {

  displayedColumns = ['Date(UTC)', 'Event', 'Device', 'Browser', 'Operation System', 'IP', 'Address'];
  dataSource: MatTableDataSource<activityData>;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  recentlyActivity: any[];

  constructor() { }

  ngOnInit() {

    const data1: activityData = {
      date: '2018-08-16 15:15',
      event: 'sign in',
      device: 'desktop',
      browser: 'chrome 68.0',
      operationSystem: 'Win10',
      IP: '255.255.255.255',
      address: 'Paris, France'
    };

    const data2: activityData = {
      date: '2018-08-16 20:20',
      event: 'sign in',
      device: 'laptop',
      browser: 'FireFox',
      operationSystem: 'Ubuntu',
      IP: '255.255.255.0',
      address: 'London, England'
    };

    const ActivityData: activityData[] = [];
    for(let i = 0; i < 50; i++){
      ActivityData.push(data1);
      ActivityData.push(data2);
    }

    this.recentlyActivity = ActivityData;

    this.dataSource = new MatTableDataSource(ActivityData);
  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // Datasource defaults to lowercase matches
    this.dataSource.filter = filterValue;
  }

}

export interface activityData{
  date: string;
  event: string;
  device: string;
  browser: string;
  operationSystem: string;
  IP: string;
  address: string;
}
