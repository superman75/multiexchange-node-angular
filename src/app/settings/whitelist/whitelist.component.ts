import { Component, OnInit, ChangeDetectorRef } from '@angular/core';

import * as $ from 'jquery';
import 'datatables.net';
import 'datatables.net-bs4';

@Component({
  selector: 'app-whitelist',
  templateUrl: './whitelist.component.html',
  styleUrls: ['./whitelist.component.scss']
})
export class WhitelistComponent implements OnInit {

  dataTable: any;
  whitelists: any[];

  constructor(private chRef: ChangeDetectorRef) { }

  ngOnInit() {
    this.whitelists = [{
      currency: 'BTC',
      name: 'aaa@btc',
      address: '1GoK6fv4tZKXFiWL9NuHiwcwsi8JAFiwGK',
      payment: ''
    }, {
      currency: 'ETH',
      name: 'bbb@eth',
      address: '0xbb9bc244d798123fde783fcc1c72d3bb8c189413',
      payment: ''
    }];
    this.chRef.detectChanges();
    const table: any = $('#balanceTable');
    this.dataTable = table.DataTable({pagingType: 'full_numbers', destroy: true});
  }

  add() {
    const menu = document.getElementsByClassName('whiteListAdd')[0] as HTMLInputElement;
    menu.classList.remove('whitelist_hidden');
    const actions = document.getElementsByClassName('whiteListStatus_lists')[0] as HTMLInputElement;
    actions.classList.add('whitelist_hidden');
  }
  back() {
    const menu = document.getElementsByClassName('whiteListAdd')[0] as HTMLInputElement;
    menu.classList.add('whitelist_hidden');
    const actions = document.getElementsByClassName('whiteListStatus_lists')[0] as HTMLInputElement;
    actions.classList.remove('whitelist_hidden');
  }
  add_address() {
    // const cont = document.getElementsByClassName('whiteListAdd__body')[0] as HTMLElement;
    // cont.htmlToAdd = '<div class="whiteListAdd__row ui-clearfix"><div class="whiteListAdd__field ui-clearfix whiteListAdd__field_name"><label for="37_name">Name</label><input type="text" id="37_name" name="name"><div class="ok"></div><div class="error"></div></div><div class="whiteListAdd__field ui-clearfix whiteListAdd__field_address"><label for="37_address">Address</label><input type="text" id="37_address" name="address"><div class="ok"></div><div class="error"></div></div><div class="whiteListAdd__field whiteListAdd__field_remove"><div class="whiteListAdd__remove">×</div></div></div>';
  }

}
