import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-apikeys',
  templateUrl: './apikeys.component.html',
  styleUrls: ['./apikeys.component.scss']
})
export class ApikeysComponent implements OnInit {
  i = 1
  constructor() { }

  ngOnInit() {
  }

  change_status() {
    const button = document.getElementsByClassName('button_apikeyActivate')[0];
    if ((this.i+1)%2 == 0){
      button.classList.add('active');
      button.classList.remove('revoked');
      this.i += 1;
    }
    else{
      button.classList.add('revoked');
      button.classList.remove('active');
      this.i += 1;
    }

  }

}
