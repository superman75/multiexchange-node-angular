import { Component, OnInit, AfterViewInit } from '@angular/core';

@Component({
  selector: 'app-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.scss']
})
export class SettingsComponent implements OnInit {

  cur_;
  sidebar_menu = [];

  constructor() { }

  ngOnInit() {
    this.cur_ = 0;
    this.sidebar_menu = ['General Settings', 'Whitelist', 'Security', 'API keys', 'Account verification', 'Affiliate'];
  }

  // ngAfterViewInit() {
  //   const menus = document.getElementsByClassName('side_bar')[0].children;
  //   (<HTMLSpanElement>menus[this.cur_]).classList.add('shadow1-text');
  // }

  changeSidebar(index) {
    this.cur_ = index;
    const menus_ = document.getElementsByClassName('side_bar')[0].children;

    for (let i = 0; i < menus_.length; i++) {
      if (i === index) {
        (<HTMLSpanElement>menus_[i]).classList.add('shadow1-text');
      } else {
        (<HTMLSpanElement>menus_[i]).classList.remove('shadow1-text');
      }
    }
  }

}
