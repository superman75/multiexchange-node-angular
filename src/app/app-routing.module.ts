import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import {MainComponent} from './main/main.component';
import { LandComponent } from './land/land.component';
import {SignUpComponent} from "./sign-up/sign-up.component";
import {LoginComponent} from "./login/login.component";
import {EmailverifyComponent} from "./emailverify/emailverify.component";
import {EmailsuccessComponent} from "./emailsuccess/emailsuccess.component";

const routes: Routes = [
  {
    path: '',
    component: LandComponent
  }, {
    path: 'main',
    component: MainComponent
  },{
    path: 'sign_up',
    component: SignUpComponent
  },{
    path: 'login',
    component: LoginComponent
  },{
    path: 'emailverify',
    component: EmailverifyComponent
  },{
    path: 'emailsuccess',
    component: EmailsuccessComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
