import { Component, OnInit } from '@angular/core';
import {DataService} from "../data.service";
import {Router} from "@angular/router";

import * as $ from 'jQuery';

@Component({
  selector: 'app-emailverify',
  templateUrl: './emailverify.component.html',
  styleUrls: ['./emailverify.component.scss']
})
export class EmailverifyComponent implements OnInit {

  keycode : string;
  data : any;
  alertMessage: string;
  alertInstance: any;
  hiddenElement : boolean = true;

  constructor(private dataService : DataService,private _router:Router) {  }

  ngOnInit() {
    this.alertInstance = $("#success-alert");
  }

  displayAlert(msg){
    this.alertMessage = msg;
    this.hiddenElement = false;
    setTimeout(()=>{
      this.hiddenElement = true;
    }, 3000);
    // this.alertInstance.delay(4000).slideUp(200, function() {
    //
    // });
    // this.alertInstance.fadeTo(2000, 500).slideUp(500, function(){
    //   $("#success-alert").slideUp(500);
    // });
  }

  verifyCode(){
    if(this.keycode == '' || this.keycode == null){
      this.displayAlert('Please insert keycode');
    }
    else {
      console.log(this.keycode);
      this.dataService.emailVerify(this.keycode).subscribe(res=>{
        this.data = res;
        if(this.data.status == 'Success')
        {
          this.displayAlert('Your email is verified succcessfully');
          this._router.navigate(['emailsuccess']);
        } else {
          console.log();
          this.displayAlert(this.data.msg);
        }

      });

    }
  }

}
